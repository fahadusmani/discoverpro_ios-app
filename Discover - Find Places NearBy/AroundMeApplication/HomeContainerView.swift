//
//  HomeContainerView.swift
//  AroundMeApplication
//
//  Created by Admin on 6/19/17.
//  Copyright © 2017 Mujadidia. All rights reserved.
//

import UIKit

class HomeContainerView: UIViewController {
    
    static var i = 0
    
    private lazy var homeViewController: HomeVc = {
        // Load Storyboard
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        
        // Instantiate View Controller
        var viewController = storyboard.instantiateViewController(withIdentifier: "HomeVc") as! HomeVc
        
        // Add View Controller as Child View Controller
        self.add(asChildViewController: viewController)
        
        return viewController
    }()
    
    private lazy var gridViewController: GridViewController = {
        // Load Storyboard
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        
        // Instantiate View Controller
        var viewController = storyboard.instantiateViewController(withIdentifier: "GridViewController") as! GridViewController
        
        // Add View Controller as Child View Controller
        self.add(asChildViewController: viewController)
        
        return viewController
    }()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("runnungg")
        //  updateView()
        
        view.backgroundColor = UIColor.gray
        // performSegue(withIdentifier: "toListVc", sender: self)
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        updateView()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func chksegment()
    {
        
    }
    
    private func add(asChildViewController viewController: UIViewController) {
        // Add Child View Controller
        addChildViewController(viewController)
        
        // Add Child View as Subview
        view.addSubview(viewController.view)
        
        // Configure Child View
        viewController.view.frame = view.bounds
        viewController.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        // Notify Child View Controller
        viewController.didMove(toParentViewController: self)
    }
    
    
    private func remove(asChildViewController viewController: UIViewController) {
        // Notify Child View Controller
        viewController.willMove(toParentViewController: nil)
        
        // Remove Child View From Superview
        viewController.view.removeFromSuperview()
        
        // Notify Child View Controller
        viewController.removeFromParentViewController()
    }
    
    
    private func updateView() {
        if HomeContainerView.i == 0 {
            remove(asChildViewController: homeViewController)
            add(asChildViewController: gridViewController)
        }
        else {
            remove(asChildViewController: gridViewController)
            add(asChildViewController: homeViewController)
        }
    }
    
    
    
    
    
}
