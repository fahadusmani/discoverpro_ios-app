//
//  HomeViewController.swift
//  AroundMeApplication
//
//  Created by Admin on 6/19/17.
//  Copyright © 2017 Mujadidia. All rights reserved.
//

import UIKit


class HomeViewController: UIViewController {
    
    
    var applyTheme = colorSelect()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        applyTheme.setNavigationTitleImage(item: self.navigationItem) //Setting Navigation Image
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
