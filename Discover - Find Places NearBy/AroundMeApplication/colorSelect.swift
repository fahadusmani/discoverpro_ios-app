//
//  colorSelect.swift
//  AroundMeApplication
//
//  Created by Admin on 4/28/17.
//  Copyright © 2017 Mujadidia. All rights reserved.
//

import Foundation
import UIKit
import CoreData

class colorSelect
{
    static var colorStyle = UIColor.blue
    static var r : Int?
    static var g : Int?
    static var b : Int?
    static var color : UIColor?
    static var titleFont = UIFont(name: "Noto Sans", size: 16.0)
    static var textFont = UIFont(name: "Noto Sans", size: 14.0)
    static var detailFont = UIFont(name: "Noto Sans", size: 13.0)
    static var titleColor = UIColor(red: CGFloat(250)/255.0, green: CGFloat(250)/255.0, blue: CGFloat(250)/255.0, alpha: 1)
    
    static var settingsTitleColor = UIColor(red: CGFloat(250)/255.0, green: CGFloat(250)/255.0, blue: CGFloat(250)/255.0, alpha: 1)
    static var code : String?
    
    func fetchColor()
    {
        var red = UserDefaults.standard.value(forKey: "red")
        var green = UserDefaults.standard.value(forKey: "green")
        var blue = UserDefaults.standard.value(forKey: "blue")
        var showMenu = UserDefaults.standard.value(forKey: "showList")
        var distanceUnit = UserDefaults.standard.value(forKey: "distanceInKm")
        var currentDis = UserDefaults.standard.value(forKey: "currentDistance")
        var currentRadius = UserDefaults.standard.value(forKey: "currentRadius")
        
        
        if (red != nil && green != nil && blue != nil)
        {
            colorSelect.r = red as! Int
            colorSelect.g = green as! Int
            colorSelect.b = blue as! Int
            
            colorSelect.color = UIColor(red: CGFloat(colorSelect.r!)/255.0, green: CGFloat(colorSelect.g!)/255.0, blue: CGFloat(colorSelect.b!)/255.0, alpha: 1.0)
            
            if(colorSelect.r == 27 && colorSelect.g == 154 && colorSelect.b == 247)
            {
                SettingVc.selected = 2
            }
            if(colorSelect.r == 103 && colorSelect.g == 58 && colorSelect.b == 183)
            {
                SettingVc.selected = 1
            }
            if(colorSelect.r == 250 && colorSelect.g == 67 && colorSelect.b == 81)
            {
                SettingVc.selected = 3
            }
            if(colorSelect.r == 0 && colorSelect.g == 151 && colorSelect.b == 167)
            {
                SettingVc.selected = 4
            }
            
            
            
        }
        else
        {
            print("else")
            colorSelect.r = 27
            colorSelect.g = 154
            colorSelect.b = 247
            SettingVc.selected = 2
            
            
            colorSelect.color = UIColor(red: CGFloat(colorSelect.r!)/255.0, green: CGFloat(colorSelect.g!)/255.0, blue: CGFloat(colorSelect.b!)/255.0, alpha: 1)
        }
        
        if(showMenu != nil)
        {
            HomeContainerView.i = showMenu as! Int
        }
        else
        {
            HomeContainerView.i = 0
        }
        
        if(distanceUnit != nil)
        {
            ListVc.distanceUnitKm = distanceUnit! as! Bool
        }
        else
        {
            ListVc.distanceUnitKm = true
        }
        
        if(currentDis != nil)
        {
            JsonRequest.distance = currentDis as! Int
            print("jdis = \(JsonRequest.distance)")
        }
        else
        {
            JsonRequest.distance = 3000
        }
        
        if(currentRadius != nil)
        {
            SettingVc.radiusType = currentRadius as! Double
        }
        else
        {
            SettingVc.radiusType = 3.0
        }
        
    }
    
    func saveColor(r : Int, g : Int, b : Int)
    {
        UserDefaults.standard.set(r, forKey: "red")
        UserDefaults.standard.set(g, forKey: "green")
        UserDefaults.standard.set(b, forKey: "blue")
    }
    
    func saveListSetting(isListEnabled : Int)
    {
        UserDefaults.standard.set(isListEnabled, forKey: "showList")
    }
    func saveDistanceSettings(disInKm: Bool)
    {
        UserDefaults.standard.set(disInKm, forKey: "distanceInKm")
    }
    func saveDistanceValue(distance : Int, radiusValue: Double)
    {
        UserDefaults.standard.set(distance, forKey: "currentDistance")
        UserDefaults.standard.set(radiusValue, forKey: "currentRadius")
    }
    
    func applyColor(nav : UINavigationController, bar: UITabBarController)
    {
        nav.navigationBar.barTintColor = colorSelect.color
        nav.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
        nav.navigationBar.tintColor = UIColor.white
        
        bar.tabBar.tintColor = colorSelect.color
        
        bar.tabBar.selectedItem?.setTitleTextAttributes([NSForegroundColorAttributeName: colorSelect.color], for: .selected)
        
        bar.tabBar.selectedItem?.setTitleTextAttributes([NSForegroundColorAttributeName: UIColor.gray], for: .normal)
    }
    
    func setNavigationTitleImage(item: UINavigationItem)
    {
        let logo = #imageLiteral(resourceName: "discover logo 1")
        let imageView = UIImageView(image:logo)
        item.titleView = imageView
    }
    
}
