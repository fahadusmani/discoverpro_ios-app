//
//  Info_Data.swift
//  AroundMeApplication
//
//  Created by Admin on 4/18/17.
//  Copyright © 2017 Mujadidia. All rights reserved.
//

import Foundation

public class Info_Data
{
    var info = [infoData]()
    
    var infoName = ["About Us","Feedbacks","Shared with a Friend","More Apps"]
    
    var iconName = ["setting info","setting feedback","setting share","settings_MoreApps"]
    
    func getData() -> [infoData]
    {
        var data = infoData()
        for i in 0...infoName.count-1
        {
            data.namee = infoName[i]
            data.icon = iconName[i]
            info.append(data)
        }
        return info
    }
}

