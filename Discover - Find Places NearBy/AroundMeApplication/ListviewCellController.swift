//
//  HomeVcModel.swift
//  AroundMeApplication
//
//  Created by Admin on 6/22/17.
//  Copyright © 2017 Mujadidia. All rights reserved.
//

import Foundation
import UIKit

class ListviewCellController : UITableView
{
    var imageObject = ImageColor()
    var menu_Data_Obj = Menu_Data()
    
    func applyCellAttributes(cell: UITableViewCell, index: Int, isSearching : Bool, text : String) -> UITableViewCell
    {
        if(isSearching)
        {
            let imgName = menu_Data_Obj.menuIconDictionary[text]
            let img = UIImage(named: imgName!)
            let newImg = imageObject.maskWithColor(img: img!, color: colorSelect.color!)
            let cellAccessoryImage = imageObject.maskWithColor(img: #imageLiteral(resourceName: "Info_DiscloserIndicator1"), color: colorSelect.color!)
            
            cell.textLabel?.font = UIFont(name: "Noto Sans", size: 17.0)
            cell.textLabel!.text = text
            cell.imageView?.image = newImg
            
            cell.accessoryType = .disclosureIndicator
            cell.accessoryView = UIImageView(image: cellAccessoryImage)
            
            cell.selectionStyle = .none
            
            return cell
        }
            
        else
        {
            let img = UIImage(named: menu_Data_Obj.menuIconDictionary[menu_Data_Obj.menuName[index]]!)
            
            let newImg = imageObject.maskWithColor(img: img!, color: colorSelect.color!)
            let cellAccessoryImage = imageObject.maskWithColor(img: #imageLiteral(resourceName: "Info_DiscloserIndicator1"), color: colorSelect.color!)
            
            cell.textLabel?.font = UIFont(name: "Noto Sans", size: 17.0)
            cell.textLabel!.text = menu_Data_Obj.menuName[index]
            //menuArray![indexPath.row].namee
            cell.imageView?.image = newImg
            
            cell.accessoryType = .disclosureIndicator
            cell.accessoryView = UIImageView(image: cellAccessoryImage)
            
            cell.selectionStyle = .none
            
            return cell
        }
    }
    
    func setDataToSend(isSearching : Bool, text : String, index: Int) -> [menuData]
    {
        var dataa = menuData()
        if(isSearching)
        {
            dataa.namee = text
            dataa.selectedKey = menu_Data_Obj.menuKeyDictionary[text]
            dataa.icon = menu_Data_Obj.menuIconDictionary[text]
            return [dataa]
        }
        else
        {
            dataa.namee = menu_Data_Obj.menuName[index]
            dataa.selectedKey = menu_Data_Obj.menuKeyDictionary[menu_Data_Obj.menuName[index]]
            dataa.icon = menu_Data_Obj.menuIconDictionary[menu_Data_Obj.menuName[index]]
            return [dataa]
        }
    }
    
}
