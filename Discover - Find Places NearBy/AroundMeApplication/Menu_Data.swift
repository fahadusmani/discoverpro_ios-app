//
//  Menu_Data.swift
//  AroundMeApplication
//
//  Created by Admin on 4/18/17.
//  Copyright © 2017 Mujadidia. All rights reserved.
//

import Foundation

public class Menu_Data
{
    var menu = [menuData]()
    
    
    var menuIconDictionary: [String: String] = [
        "Accounting": "Accounting",
        "Airport": "plane",
        "Amusement Park": "amusement park",
        "Aquarium": "Aquarium",
        "Art Gallery": "Art_Gallery",
        "Atm": "Atm",
        "Bank": "bank",
        "Bakery": "bakery",
        "Beauty Salon": "beauty_saloon",
        "Barber Shop": "beauty_saloon",
        "Bicycle Store": "Bicycle_Store",
        "Book Store": "book store",
        "Bowling Alley": "Bowling_Alley",
        "Bus Station": "bus station",
        "Cafe": "cafe",
        "Campground": "Campground",
        "Car Dealer": "Car_Dealer",
        "Car Rental": "Car_Rental",
        "Car Workshop": "car_repair",
        "Car Wash": "car_wash",
        "Casino": "casino",
        "Cemetery": "Cemetery",
        "Church": "church",
        "Clothing Store": "Clothing_Store",
        "Convenience Store": "Convenience_store",
        "Courthouse": "Courthouse",
        "Dentist": "dentist",
        "Departmental Store": "Departmental_Store",
        "Doctor": "doctor",
        "Electrician": "electrician",
        "Electronics Store": "electronic_store",
        "Embassy": "Embassy",
        "Fire Station": "Fire_Station",
        "Florist": "Florist",
        "Funeral Home": "Cemetery",
        "Furniture Store": "Furniture_Store",
        "Gas Station": "gas station",
        "Gym": "gym",
        "Hardware Store": "Hardware_Store",
        "Home Goods Store": "bank",
        "Hospital": "hospital",
        "Insurance Agency": "Insurance_Agency",
        "Jewelery Store": "Jewelery_Store",
        "Laundry": "Laundry",
        "Lawyer": "Lawyer",
        "Library": "library",
        "Local Government Office": "bank",
        "Meal Delivery": "delivery",
        
        "Mosque": "mosque",
        "Movie Rental": "Movie_Theater",
        "Movie Theater": "Movie_Theater",
        "Meuseum": "Meuseum",
        "Painter": "Painter",
        "Park": "Park",
        "Parking": "Parking",
        "Pet Store": "Pet_Store",
        "Pharmacy": "pharmacy",
        "Physiotherapist": "Physiotherapist",
        "Plumber": "Plumber",
        "Police": "police station",
        "Post Office": "post office",
        "Real Estate Agency": "Real_Estate_Agency",
        "Restuarant": "restuarant",
        
        "School": "school",
        "Shoe Store": "Shoe_Store",
        "Shopping Mall": "supermarket",
        "Stadium": "Stadium",
        
        "Store": "Departmental_Store",
        "Subway Station": "Subway_Station",
        "Taxi Stand": "Taxi_Stand",
        "Station": "bank",
        
        "Travel Agency": "plane",
        "University": "school",
        "Zoo": "Zoo",
        "Temple": "Temple"
    ]
    
    var menuKeyDictionary: [String: String] = [
        "Accounting": "accounting",
        "Airport": "airport",
        "Amusement Park": "amusement_park",
        "Aquarium": "aquarium",
        "Art Gallery": "art_gallery",
        "Atm": "atm",
        "Bank": "bank",
        "Bakery": "bakery",
        "Beauty Salon": "beauty_salon",
        "Barber Shop": "hair_care",
        "Bicycle Store": "bicycle_store",
        "Book Store": "book_store",
        "Bowling Alley": "bowling_alley",
        "Bus Station": "bus_station",
        "Cafe": "cafe",
        "Campground": "campground",
        "Car Dealer": "car_dealer",
        "Car Rental": "car_rental",
        "Car Workshop": "car_repair",
        "Car Wash": "car_wash",
        "Casino": "casino",
        "Cemetery": "cemetery",
        "Church": "church",
        "Clothing Store": "clothing_store",
        "Convenience Store": "convenience_store",
        "Courthouse": "courthouse",
        "Dentist": "dentist",
        "Departmental Store": "department_store",
        "Doctor": "doctor",
        "Electrician": "electrician",
        "Electronics Store": "electronics_store",
        "Embassy": "embassy",
        "Fire Station": "fire_station",
        "Florist": "florist",
        "Funeral Home": "funeral_home",
        "Furniture Store": "furniture_store",
        "Gas Station": "gas_station",
        "Gym": "gym",
        "Hardware Store": "hardware_store",
        "Home Goods Store": "home_goods_store",
        "Hospital": "hospital",
        "Insurance Agency": "insurance_agency",
        "Jewelery Store": "jewelry_store",
        "Laundry": "laundry",
        "Lawyer": "lawyer",
        "Library": "library",
        "Local Government Office": "local_government_office",
        "Meal Delivery": "meal_delivery",
        
        "Mosque": "mosque",
        "Movie Rental": "movie_rental",
        "Movie Theater": "movie_theater",
        "Moving Company": "moving_company",
        "Meuseum": "museum",
        "Painter": "painter",
        "Park": "park",
        "Parking": "parking",
        "Pet Store": "pet_store",
        "Pharmacy": "pharmacy",
        "Physiotherapist": "physiotherapist",
        "Plumber": "plumber",
        "Police": "police",
        "Post Office": "post_office",
        "Real Estate Agency": "real_estate_agency",
        "Restuarant": "restaurant",
        
        "School": "school",
        "Shoe Store": "shoe_store",
        "Shopping Mall": "shopping_mall",
        "Stadium": "stadium",
        
        "Store": "store",
        "Subway Station": "subway_station",
        "Taxi Stand": "taxi_stand",
        "Station": "train_station",
        
        "Travel Agency": "travel_agency",
        "University": "university",
        "Zoo": "zoo",
        "Temple": "hindu_temple"
    ]
    
    
    var menuName = ["Accounting","Airport","Atm","Amusement Park","Aquarium","Art Gallery","Bank","Bakery","Beauty Salon","Barber Shop","Bicycle Store","Book Store","Bowling Alley","Bus Station","Cafe","Campground","Car Dealer","Car Rental","Car Workshop","Car Wash","Casino","Cemetery","Church","Clothing Store","Convenience Store","Courthouse","Dentist","Departmental Store","Doctor","Electrician","Electronics Store","Embassy","Fire Station","Florist","Funeral Home","Furniture Store","Gas Station","Gym","Hardware Store","Home Goods Store","Hospital","Insurance Agency","Jewelery Store","Laundry","Lawyer","Library","Local Government Office","Meal Delivery","Mosque","Movie Rental","Movie Theater","Meuseum","Painter","Park","Parking","Pet Store","Pharmacy","Physiotherapist","Plumber","Police","Post Office","Real Estate Agency","Restuarant","School","Shoe Store","Shopping Mall","Stadium","Store","Subway Station","Taxi Stand","Station","Temple","Travel Agency","University","Zoo"]
    
    
    var iconName = ["plane","amusement park","bank","bakery","book store","bus station","bus station","bank","bank","bank","dentist","restuarant","gym","gas station",
                    "supermarket","hospital","library","mosque","bank","pharmacy","police station","post office","school","bank","bank"]
    
    var keyName = ["airport","amusement_park","bank","bakery","book_store","bus_station","fire_station","cafe","church","car_wash","dentist","restaurant","gym","gas_station","shopping_mall","hospital","library","mosque","hindu_temple","pharmacy","police","post_office","school","university","stadium"]
    
    func getData() -> [menuData]
    {
        var data = menuData()
        for i in 0...menuName.count-1
        {
            data.namee = menuName[i]
            //  data.icon = iconName[i]
            //  data.selectedKey = keyName[i]
            menu.append(data)
        }
        return menu
    }
}

