//
//  ViewController.swift
//  AroundMeApplication
//
//  Created by Admin on 4/18/17.
//  Copyright © 2017 Mujadidia. All rights reserved.
//

import UIKit
import Foundation
import SystemConfiguration
import GoogleMobileAds

class HomeVc: UIViewController, UITableViewDataSource, UITableViewDelegate,  UISearchBarDelegate {
    
    var tap : UIGestureRecognizer?
    var objColor = colorSelect()  //class objects
    var map = LoadMap()
    var menu_Data_Obj = Menu_Data()
    var imageObject = ImageColor()
    var alertObj = AlertViewws()
    var checkInternet = CheckInternetServices()
    var cellController = ListviewCellController()
    
    @IBOutlet weak var searchBarLabel: UILabel!
    @IBOutlet weak var searchBarLabelView: UIView!
    @IBOutlet weak var table: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    static var isLocationEnabled = true
    var filteredData : [String] = []
    var data = [String]()
    var isSearching = false
    var text : String?
    var colorTheme : UIColor?       //class variables
    var menuArray : [menuData]?
    var sendData : [menuData]?
    var searchData : [String] = []
    var dataa : [String] = []
    var y = 0
    var z = 0
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if(isSearching)
        {
            return filteredData.count
        }
        else
        {
            return (menuArray?.count)!
        }
    }
    
    // show table view
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        table.isHidden = false
        if(isSearching)
        {
            text = filteredData[indexPath.row]
            
            let cellIdentifier = "cell"
            var cell : UITableViewCell  = tableView.dequeueReusableCell(withIdentifier: cellIdentifier)! as UITableViewCell
            cell = cellController.applyCellAttributes(cell: cell, index: indexPath.row, isSearching: true, text: text!)
            
            return cell
        }
            
        else
        {
            //view.endEditing(true)
            if(searchBar.text == "")
            {
                view.endEditing(true)
                if let tapp = tap
                {
                    self.view.removeGestureRecognizer(tap!)
                }
            }
            else
            {
                table.isHidden = true
                // searchBarLabelView.isHidden = false
                fadeViewInThenOut(view: searchBarLabelView, delay: 1.5)
                searchBarLabelView.backgroundColor = colorSelect.settingsTitleColor
                
            }
            
            
            text = menuArray?[indexPath.row].namee
            let cellIdentifier = "cell"
            var cell : UITableViewCell  = tableView.dequeueReusableCell(withIdentifier: cellIdentifier)! as UITableViewCell
            cell = cellController.applyCellAttributes(cell: cell, index: indexPath.row, isSearching: false, text: "")
            
            return cell
        }
    }
    
    func searchBarTextDidBeginEditing(searchBar: UISearchBar) {
        isSearching = true;
    }
    
    func searchBarTextDidEndEditing(searchBar: UISearchBar) {
        isSearching = false;
    }
    
    func searchBarCancelButtonClicked(searchBar: UISearchBar) {
        isSearching = false;
    }
    
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
        tap = UITapGestureRecognizer(target: self, action: "dismissKeyboard")
        view.addGestureRecognizer(tap!)
        
        return true
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        dismissKeyboard()
        fadeViewInThenOut(view: searchBarLabelView, delay: 1.5)
        searchBarLabelView.backgroundColor = colorSelect.settingsTitleColor
    }

    
    //pod 'SwiftyJSON', :git => 'https://github.com/acegreen/SwiftyJSON.git', :branch => 'swift3'
    
    
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.view.removeGestureRecognizer(tap!)
        y = 0
        filteredData = data.filter({ (text) -> Bool in
            let tmp: NSString = text as NSString
            let range = tmp.range(of: searchText, options: NSString.CompareOptions.caseInsensitive)
            
            if(range.length != 0)
            {
                y += 1
            }
            
            return range.location != NSNotFound
        })
        
        if(filteredData.count == 0){
            if(z>2 && searchBar.text != "")
            {
                filteredData = dataa
                isSearching = true
            }
            else{
                isSearching = false
                z = 0
                dataa = []
            }
        }
        else
        {
            dataa = filteredData
            isSearching = true
            z = z + 1
        }
        table.reloadData()
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool // called when 'return' key pressed. return false to ignore.
    {
        textField.resignFirstResponder()
        return true
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let internetConnection = checkInternet.isInternetAvailable()
        
        if(internetConnection == true)
        {
            ListVc.lastPage = 0
            JsonData.nextPageToken = ""
            if(isSearching)
            {
                text = filteredData[indexPath.row]
                sendData = cellController.setDataToSend(isSearching: true, text: text!, index: indexPath.row)
            }
            else
            {
                sendData = cellController.setDataToSend(isSearching: false, text: "", index: indexPath.row)
            }
            performSegue(withIdentifier: "toListVc", sender: self)
        }
        else
        {
            let alert = alertObj.setInternetAlert()
            present(alert, animated: true, completion: nil)
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        UIApplication.shared.statusBarStyle = .lightContent
        
        searchBar.delegate = self
        searchBar.returnKeyType = UIReturnKeyType.search
        searchBar.placeholder = "Search"
        menuArray = menu_Data_Obj.getData()     //Loading search categories
        data = menu_Data_Obj.menuName
        map.loadGMS()                           //Loading Current Location
        objColor.setNavigationTitleImage(item: self.navigationItem)
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        self.tabBarController?.tabBar.isHidden = false
        
        objColor.fetchColor()           //applying theme
        objColor.applyColor(nav: self.navigationController!, bar: self.tabBarController!)
        table.reloadData()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        isSearching = false
        searchBar.text = ""
        dismissKeyboard()
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if(segue.identifier == "toListVc")
        {
            var dest = segue.destination as! ListVc
            dest._getDataArray = sendData
        }
    }
    
    func fadeViewInThenOut(view : UIView, delay: TimeInterval) {
        
        let animationDuration = 1.5
        
        // Fade in the view
        UIView.animate(withDuration: animationDuration, animations: { () -> Void in
            view.alpha = 1
        }) { (Bool) -> Void in
            self.searchBarLabelView.isHidden = false
            self.searchBarLabelView.layer.cornerRadius = 6.0
            self.searchBarLabel.text = "No match found"
            // After the animation completes, fade out the view after a delay
            
            UIView.animate(withDuration: animationDuration, delay: delay, options: .curveEaseInOut, animations: { () -> Void in
                view.alpha = 0
            },
                           completion: nil)
        }
    }

    func dismissKeyboard() {
        view.endEditing(true)
        if let tapp = tap
        {
            self.view.removeGestureRecognizer(tap!)
        }
    }

    
    
    
    
}


extension UIApplication {
    var statusBarView: UIView? {
        return value(forKey: "statusBar") as? UIView
    }
}

extension UINavigationController
{
    override open var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}

