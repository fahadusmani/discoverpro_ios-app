//
//  JsonRequest.swift
//  AroundMeApplication
//
//  Created by Admin on 4/24/17.
//  Copyright © 2017 Mujadidia. All rights reserved.
//

import Foundation

protocol JsonRequestDelegate: class
{
    func sendData(data: [Info])
    func sendData1(data: [ListDataArray])
}

class JsonRequest
{
    weak var delegate : JsonRequestDelegate?
    let jsonDataObj = JsonData()
    let mapObj = LoadMap()
    var places = [placeData]()
    var r = [Info]()
    var _receivedJsonData = [Info]()
    var _receivedJsonData1 = [ListDataArray]()
    var _placeData = [placeData]()
    let longitude = LoadMap.longitude
    let latitude = LoadMap.latitude
    
    static var distance = 0
    static var newJsonData = [Info]()
    static var count : Int?
    
    func update(_key : String)
    {
        print("dis = \(JsonRequest.distance)")
        let searchKey = _key
        let config = URLSessionConfiguration.default // Session Configuration
        let session = URLSession(configuration: config) // Load configuration into Session
        
        // print("TOKEN= \(JsonData.nextPageToken)")
        
        let url = URL(string: "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=\(latitude!),\(longitude!)&radius=\(JsonRequest.distance)&type=\(searchKey)&key=AIzaSyCe5_-E3lm6HLJ1rQZ0KJ90-KNc5Qvhmzk&pagetoken=\(JsonData.nextPageToken)")!
        
        
        let task = session.dataTask(with: url, completionHandler: {
            (data, response, error) in
            
            if error != nil
            {
                print(error!.localizedDescription)
            }
                
            else
            {
                do {
                    if let json = try JSONSerialization.jsonObject(with: data!, options: .allowFragments) as? [String: Any]
                    {
                        self._receivedJsonData.removeAll()
                        self._receivedJsonData = self.jsonDataObj.readJSONObject(object: json)
                        JsonRequest.count = self._receivedJsonData.count-1
                        self.delegate?.sendData(data: self._receivedJsonData)
                    }
                }
                catch
                {
                    print("error in JSONSerialization")
                }
            }
        })
        task.resume()
    }
    
    func updateShedule(pid : String)
    {
        var id = pid
        let config = URLSessionConfiguration.default // Session Configuration
        let session = URLSession(configuration: config) // Load configuration into Session
        
        let url = URL(string: "https://maps.googleapis.com/maps/api/place/details/json?placeid=\(id)&key=AIzaSyCe5_-E3lm6HLJ1rQZ0KJ90-KNc5Qvhmzk")!
        
        let task = session.dataTask(with: url, completionHandler: {
            (data, response, error) in
            
            if error != nil
            {
                print(error!.localizedDescription)
            }
            else
            {
                do {
                    if let json = try JSONSerialization.jsonObject(with: data!, options: .allowFragments) as? [String: Any]
                    {
                        self._receivedJsonData1 = self.jsonDataObj.readJSONObject1(object: json)
                        self.delegate?.sendData1(data: self._receivedJsonData1)
                    }
                }
                catch
                {
                    print("error in JSONSerialization")
                }
            }
        })
        task.resume()
    }
}
