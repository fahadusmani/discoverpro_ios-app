//
//  AboutVc.swift
//  AroundMeApplication
//
//  Created by Admin on 5/15/17.
//  Copyright © 2017 Mujadidia. All rights reserved.
//
import UIKit

class AboutVc: UIViewController
{
    var imageObject = ImageColor()
    var objColor = colorSelect()
    
    @IBOutlet weak var fbBtn: UIButton!
    @IBOutlet weak var twitterBtn: UIButton!
    @IBOutlet weak var mailBtn: UIButton!
    
    @IBAction func fbBtn(_ sender: Any)
    {
        UIApplication.shared.openURL(URL(string:"https://www.facebook.com/MujadidiaInc/")!)
    }
    
    @IBAction func twitterBtn(_ sender: Any)
    {
        UIApplication.shared.openURL(URL(string:"https://twitter.com/MujadidiaInc")!)
    }
    
    @IBAction func MailBtn(_ sender: Any)
    {
        let email = "contact@mujadidia.com"
        if let url = URL(string: "mailto:\(email)") {
            UIApplication.shared.open(url)
        }
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        objColor.setNavigationTitleImage(item: self.navigationItem)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.tabBarController?.tabBar.isHidden = false
        objColor.fetchColor()
        objColor.applyColor(nav: navigationController!, bar: tabBarController!)
        
        fbBtn.setImage(imageObject.maskWithColor(img: #imageLiteral(resourceName: "fb"), color: colorSelect.color!), for: .normal)
        twitterBtn.setImage(imageObject.maskWithColor(img: #imageLiteral(resourceName: "twitter"), color: colorSelect.color!), for: .normal)
        mailBtn.setImage(imageObject.maskWithColor(img: #imageLiteral(resourceName: "mail"), color: colorSelect.color!), for: .normal)
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
}
