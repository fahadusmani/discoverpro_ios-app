//
//  CellData .swift
//  AroundMeApplication
//
//  Created by Admin on 5/18/17.
//  Copyright © 2017 Mujadidia. All rights reserved.
//

import Foundation
import UIKit

class detailCellController
{
    var imageObj = ImageColor()
    var coreDataObject = CoreDataModel()
    var array = [DetailDataArray]()
    var array1 = [ListDataArray]()
    
    
    func getButtonCellData(cell: UITableViewCell) -> UITableViewCell
    {
        let newCell = cell as! ButtonCell
        var setFavoriteImage = imageObj.maskWithColor(img: #imageLiteral(resourceName: "map fav blank"), color: colorSelect.color!)
        DetailVc.isFavorite = false
        let favoriteArray = coreDataObject.fetchData()
        
        if(favoriteArray.count > 0)
        {
            for i in 0...favoriteArray.count-1
            {
                if(array[0].name == favoriteArray[i].name)
                {
                    setFavoriteImage = imageObj.maskWithColor(img: #imageLiteral(resourceName: "map fav fill"), color: colorSelect.color!)
                    DetailVc.isFavorite = true
                    break
                }
            }
        }
        
        let favoriteImg = setFavoriteImage //imageObj.maskWithColor(img: #imageLiteral(resourceName: "map fav fill"), color: colorSelect.color!)
        let shareImg = imageObj.maskWithColor(img: #imageLiteral(resourceName: "map share icon"), color: colorSelect.color!)
        let locationImg = imageObj.maskWithColor(img: #imageLiteral(resourceName: "map location"), color: colorSelect.color!)
        
        newCell.favoriteButtonn.setImage(favoriteImg, for: .normal)
        newCell.shareButton.setImage(shareImg, for: .normal)
        newCell.locationButton.setImage(locationImg, for: .normal)
        return newCell
    }
    
    
    func getCellData(cell: UITableViewCell, index: Int) -> UITableViewCell
    {
        var getDataArray = array
        var _receivedPlaceData = array1
        var newCell = cell
        newCell = setBodyFonts(cell: newCell)
        let row = index
        
        if(row == 10 || row == 11 || row == 12 || row == 13 || row == 14 || row == 15 || row == 16 || row == 17)
        {
            if(row == 10)
            {
                if(getDataArray[0].openNow == true)
                {
                    let cellImage = imageObj.maskWithColor(img: #imageLiteral(resourceName: "detail_clock"), color: colorSelect.color!)
                    newCell.textLabel?.text = "Opened!"
                    newCell.imageView?.image = cellImage
                }
                else
                {
                    let cellImage = imageObj.maskWithColor(img: #imageLiteral(resourceName: "detail_clock"), color: UIColor.gray)
                    newCell.textLabel?.text = "Closed!"
                    newCell.imageView?.image = cellImage
                }
            }
            else
            {
                newCell.textLabel?.font = UIFont(name: "Noto Sans", size: 12.0)
                let cellImage = imageObj.maskWithColor(img: #imageLiteral(resourceName: "detail_clock"), color: colorSelect.color!)
                if(_receivedPlaceData.count > 0 && _receivedPlaceData[0].shedule! != [])
                {
                    let placeShedule = _receivedPlaceData[0].shedule
                    newCell.textLabel?.text = placeShedule?[row-11]
                    newCell.imageView?.image = cellImage
                }
            }
        }
        
        
        
        if(row == 2)
        {
            newCell.textLabel?.text = getDataArray[0].location
            newCell.textLabel?.lineBreakMode = NSLineBreakMode.byWordWrapping
            newCell.textLabel?.numberOfLines = 0
            newCell.imageView?.image = nil
        }
        
        if(row == 4)
        {
            cell.textLabel?.text = "Open Maps"
            let img = imageObj.maskWithColor(img: #imageLiteral(resourceName: "detail_map_location"), color: colorSelect.color!)
            cell.imageView?.image = img
        }
        
        if(row == 5)
        {
            let img = imageObj.maskWithColor(img: #imageLiteral(resourceName: "detail_link"), color: colorSelect.color!)
            if(_receivedPlaceData.count > 0 && _receivedPlaceData[0].website != "")
            {
                newCell.textLabel?.font = colorSelect.textFont
                newCell.textLabel?.text = _receivedPlaceData[0].website
                newCell.imageView?.image = img
            }
            
        }
        
        if(row == 6)
        {
            let img = imageObj.maskWithColor(img: #imageLiteral(resourceName: "detail_call_icon"), color: colorSelect.color!)
            if(_receivedPlaceData.count > 0 && _receivedPlaceData[0].phone != "")
            {
                newCell.backgroundColor = UIColor.white
                newCell.textLabel?.font = colorSelect.textFont
                newCell.textLabel?.text = _receivedPlaceData[0].phone
                newCell.imageView?.image = img
            }
        }
        
        if(row == 8)
        {
            let img = imageObj.maskWithColor(img: #imageLiteral(resourceName: "detail_distance"), color: colorSelect.color!)
            cell.imageView?.image = img
            if(ListVc.distanceUnitKm)
            {
                cell.textLabel?.text = "\(getDataArray[0].distance!) KiloMeters"
            }
            else
            {
                cell.textLabel?.text = "\(getDataArray[0].distance!) Miles"
            }
        }
        
        return newCell
    }
    
    
    func getSettingsTableData(cell: UITableViewCell, row: Int) -> UITableViewCell
    {
        var newCell = cell
        if (row == 0 || row == 2 || row == 4 || row == 6 || row == 8)
        {
            newCell.textLabel?.font = colorSelect.titleFont
            newCell.textLabel?.textColor = colorSelect.color
            
            switch row
            {
            case 0:
                cell.textLabel?.text = "Theme"
                break
                
            case 2:
                cell.textLabel?.text = "Units"
                break
                
            case 4:
                cell.textLabel?.text = "Radius"
                break
                
            default:
                break
            }
            return newCell
        }
            
        else
        {
            newCell.textLabel?.font = colorSelect.textFont
            newCell.backgroundColor = UIColor.white
            newCell.backgroundColor = colorSelect.settingsTitleColor
            
            switch row
            {
                //  case 1:
                //    cell.textLabel?.text = "Upgrade to AD-free Nearby"
                //  break
                
                /*   case 9:
                 cell.textLabel?.text = "Terms of Service"
                 break
                 
                 case 10:
                 cell.textLabel?.text = "Privacy Policy"
                 break*/
                
            default:
                break
            }
            return cell
        }
    }
    
    
    
    func setTitleFonts(cell: UITableViewCell) -> UITableViewCell
    {
        cell.textLabel?.font = colorSelect.titleFont
        cell.textLabel?.textColor = colorSelect.color
        cell.backgroundColor = colorSelect.titleColor
        cell.textLabel?.backgroundColor = colorSelect.titleColor
        return cell
    }
    
    
    func setBodyFonts(cell: UITableViewCell) -> UITableViewCell
    {
        cell.textLabel?.font = colorSelect.textFont
        cell.textLabel?.textColor = UIColor.black
        cell.backgroundColor = UIColor.white
        return cell
    }
    
    
}
