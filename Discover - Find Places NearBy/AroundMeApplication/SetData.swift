//
//  SetData.swift
//  AroundMeApplication
//
//  Created by Admin on 5/17/17.
//  Copyright © 2017 Mujadidia. All rights reserved.
//

import Foundation
import UIKit

class SetData
{
    var mapObj = LoadMap()
    func favoriteData(array: [DetailDataArray], array1: [ListDataArray]) -> (DetailDataArray)
    {
        var data = DetailDataArray()
        
        data.name = array[0].name
        data.location = array[0].location
        data.latitude = array[0].latitude
        data.longitude = array[0].longitude
        data.count = array[0].count
        data.openNow = array[0].openNow
        data.website = array1[0].website
        data.phone = array1[0].phone
        data.placeId = array[0].placeId
        data.distance = mapObj.calculateDistance(lat: array[0].latitude!, long: array[0].longitude!)
        data.timings = array1[0].shedule
        
        return data
    }
    
    
    
    func detailData(array: [Info], row: Int) -> [DetailDataArray]
    {
        var x = 0
        var getData = DetailDataArray()
        getData.name = array[row].name!
        getData.location = array[row].location!
        getData.latitude = array[row].geometryArray?[0].location_latitude!
        getData.longitude = array[row].geometryArray?[0].location_longitude!
        getData.openNow = array[row].open
        getData.placeId = array[row].id
        getData.distance = mapObj.calculateDistance(lat: (array[row].geometryArray?[0].location_latitude!)!, long: (array[row].geometryArray?[0].location_longitude!)!)
        getData.count = x
        let dataArray = getData
        
        return [dataArray]
    }
    
    
    func favToDetail(array: [DetailDataArray], row: Int) -> [DetailDataArray]
    {
        var data = DetailDataArray()
        data.latitude = array[row].latitude
        data.longitude = array[row].longitude
        data.name = array[row].name
        data.phone = array[row].phone
        data.website = array[row].website
        data.openNow = array[row].openNow
        data.location = array[row].location
        data.placeId = array[row].placeId
        data.distance = mapObj.calculateDistance(lat: (array[row].latitude!), long: (array[row].longitude!))
        data.timings = array[row].timings
        
        return [data]
    }
}
