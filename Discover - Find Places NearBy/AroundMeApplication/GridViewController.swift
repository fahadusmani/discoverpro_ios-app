//
//  GridViewController.swift
//  AroundMeApplication
//
//  Created by Admin on 6/19/17.
//  Copyright © 2017 Mujadidia. All rights reserved.
//

import UIKit

class GridViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UISearchBarDelegate {
    
    
    @IBOutlet weak var searchBarLabel: UILabel!
    @IBOutlet weak var searchBarLabelView: UIView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var GridCollectionView: UICollectionView!
    
    var tap : UIGestureRecognizer?
    var filteredData : [String] = []
    var data = [String]()
    var isSearching = false
    var text : String?
    var searchData : [String] = []
    var y = 0
    var z = 0
    
    var checkInternet = CheckInternetServices()
    var cellController = GridCellController()
    var objColor = colorSelect()
    var menuDataObject = Menu_Data()
    var colorDataObject = ImageColor()
    var alertObj = AlertViewws()
    var sendData : [menuData]?
    var dataa : [String] = []
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        GridCollectionView.delegate = self
        GridCollectionView.dataSource = self
        
        searchBar.delegate = self
        searchBar.returnKeyType = UIReturnKeyType.search
        searchBar.placeholder = "Search"
        data = menuDataObject.menuName
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        self.tabBarController?.tabBar.isHidden = false
        
        objColor.fetchColor()           //applying theme
        objColor.applyColor(nav: self.navigationController!, bar: self.tabBarController!)
        GridCollectionView.reloadData()
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        isSearching = false
        searchBar.text = ""
        dismissKeyboard()
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if(isSearching)
        {
            return filteredData.count
        }
        else
        {
            return menuDataObject.menuName.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        collectionView.isHidden = false
        searchBarLabelView.isHidden = true
        collectionView.showsVerticalScrollIndicator = false
        collectionView.showsHorizontalScrollIndicator = false
        
        var cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GridCell", for: indexPath) as! GridCollectionViewCell
        
        if(isSearching)
        {
            text = filteredData[indexPath.row]      //sending cell to modify data
            cell = cellController.applyCellAttributes(cell: cell, index: indexPath.row, isSearching: true, text: text!) as! GridCollectionViewCell
            
            if let layout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout
            {
                layout.scrollDirection = .vertical
            }
            return cell
        }
            
        else
        {
            cell = cellController.applyCellAttributes(cell: cell, index: indexPath.row, isSearching: false, text: "") as! GridCollectionViewCell
            if(searchBar.text == "")
            {
                view.endEditing(true)
                if let tapp = tap
                {
                    self.view.removeGestureRecognizer(tap!)
                }
            }
            else
            {
                collectionView.isHidden = true
                // searchBarLabelView.isHidden = false
                fadeViewInThenOut(view: searchBarLabelView, delay: 1.5)
                searchBarLabelView.backgroundColor = colorSelect.settingsTitleColor
                
            }
            if let layout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout
            {
                layout.scrollDirection = .vertical
            }
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        JsonData.nextPageToken = ""
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GridCell", for: indexPath) as! GridCollectionViewCell
        
        let internetConnection = checkInternet.isInternetAvailable()
        
        if(internetConnection == true)
        {
            ListVc.lastPage = 0
            if(isSearching)
            {
                
                text = filteredData[indexPath.row]
                sendData = cellController.setDataToSend(isSearching: true, text: text!, index: indexPath.row)
            }
            else
            {
                sendData = cellController.setDataToSend(isSearching: false, text: "", index: indexPath.row)
            }
            performSegue(withIdentifier: "toListVc", sender: self)
        }
            
        else
        {
            let alert = alertObj.setInternetAlert()
            present(alert, animated: true, completion: nil)
        }
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.view.removeGestureRecognizer(tap!)
        y = 0
        filteredData = data.filter({ (text) -> Bool in
            let tmp: NSString = text as NSString
            let range = tmp.range(of: searchText, options: NSString.CompareOptions.caseInsensitive)
            
            if(range.length != 0)
            {
                y += 1
            }
            
            return range.location != NSNotFound
        })
        
        if(filteredData.count == 0){
            if(z>2 && searchBar.text != "")
            {
                filteredData = dataa
                isSearching = true
            }
            else{
                isSearching = false
                z = 0
                dataa = []
            }
            
        }
        else
        {
            dataa = filteredData
            isSearching = true
            z = z + 1
        }
        GridCollectionView.reloadData()
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool // called when 'return' key pressed. return false to ignore.
    {
        print("return")
        textField.resignFirstResponder()
        return false
    }
    
    
    
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        print("cancelled")
    }
    
    func searchBarTextDidBeginEditing(searchBar: UISearchBar) {
        print("Begin")
        isSearching = true;
    }
    
    func searchBarTextDidEndEditing(searchBar: UISearchBar) {
        print("End")
        isSearching = false;
    }
    

    
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
        tap = UITapGestureRecognizer(target: self, action: "dismissKeyboard")
        view.addGestureRecognizer(tap!)
        
        return true
    }

    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        dismissKeyboard()
        fadeViewInThenOut(view: searchBarLabelView, delay: 1.5)
        searchBarLabelView.backgroundColor = colorSelect.settingsTitleColor
    }

    
    
    //preparing data for segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if(segue.identifier == "toListVc")
        {
            var dest = segue.destination as! ListVc
            dest._getDataArray = sendData
        }
    }
    
    
    func fadeViewInThenOut(view : UIView, delay: TimeInterval) {
        
        let animationDuration = 1.5
        
        // Fade in the view
        UIView.animate(withDuration: animationDuration, animations: { () -> Void in
            view.alpha = 1
        }) { (Bool) -> Void in
            self.searchBarLabelView.isHidden = false
            self.searchBarLabelView.layer.cornerRadius = 6.0
            self.searchBarLabel.text = "No match found"
            // After the animation completes, fade out the view after a delay
            
            UIView.animate(withDuration: animationDuration, delay: delay, options: .curveEaseInOut, animations: { () -> Void in
                view.alpha = 0
            },
                           completion: nil)
        }
    }
    
    func dismissKeyboard() {
        view.endEditing(true)
        if let tapp = tap
        {
            self.view.removeGestureRecognizer(tap!)
        }
    }
    
    
    
}
