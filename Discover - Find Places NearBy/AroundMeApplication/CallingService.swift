//
//  CallingService.swift
//  AroundMeApplication
//
//  Created by Admin on 5/17/17.
//  Copyright © 2017 Mujadidia. All rights reserved.
//

import Foundation
import UIKit

class CallingService
{
    func makeCall(phone : String)
    {
        let formatedNumber = phone.components(separatedBy: NSCharacterSet.decimalDigits.inverted).joined(separator: "")
        let phoneUrl = "tel://+\(formatedNumber)"
        if let phoneCallURL = URL(string: phoneUrl)
        {
            let application:UIApplication = UIApplication.shared
            if (application.canOpenURL(phoneCallURL))
            {
                application.open(phoneCallURL, options: [:], completionHandler: nil)
            }
        }
    }
    
    
    func openWebsite(website : String)
    {
        UIApplication.shared.openURL(URL(string:"\(website)")!)
    }
    
}
