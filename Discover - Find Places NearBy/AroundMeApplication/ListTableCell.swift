//
//  ListTableCell.swift
//  AroundMeApplication
//
//  Created by Admin on 5/19/17.
//  Copyright © 2017 Mujadidia. All rights reserved.
//

import UIKit

protocol ListCellDelegate
{
    
}


class ListTableCell: UITableViewCell {
    var listDelegate: ListCellDelegate?
    
    @IBOutlet weak var distanceLabel: UILabel!
    
    @IBOutlet weak var dataLabel: UILabel!
    
    @IBOutlet weak var cellImage: UIImageView!
    
    @IBOutlet weak var cellDetailData: UILabel!
    
    @IBOutlet weak var distanceImage: UIImageView!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
