//
//  ButtonCell.swift
//  AroundMeApplication
//
//  Created by Admin on 4/25/17.
//  Copyright © 2017 Mujadidia. All rights reserved.
//

import Foundation
import UIKit

protocol ButtonCellDelegate {
    func cellTapped(cell: ButtonCell)
}

class ButtonCell: UITableViewCell {
    
    var buttonDelegate: ButtonCellDelegate?
    
    @IBAction func buttonTap(sender: UIButton) {
        if let delegate = buttonDelegate {
            delegate.cellTapped(cell: self)
        }
    }
    
}
