//
//  TableViewCelll.swift
//  AroundMeApplication
//
//  Created by Admin on 5/8/17.
//  Copyright © 2017 Mujadidia. All rights reserved.
//

import UIKit



class TableViewCelll: UITableViewCell
{
    @IBOutlet fileprivate weak var collectionView: UICollectionView!
}

extension TableViewCelll
{
    func setCollectionViewDataSourceDelegate<D: UICollectionViewDataSource & UICollectionViewDelegate>(_ dataSourceDelegate: D, forRow row: Int)
    {
        collectionView.delegate = dataSourceDelegate
        collectionView.dataSource = dataSourceDelegate
        collectionView.tag = row
        collectionView.setContentOffset(collectionView.contentOffset, animated:false) // Stops collection view if it was scrolling.
        collectionView.reloadData()
    }
    
    var collectionViewOffset: CGFloat
    {
        set { collectionView.contentOffset.x = newValue }
        get { return collectionView.contentOffset.x }
    }
}
