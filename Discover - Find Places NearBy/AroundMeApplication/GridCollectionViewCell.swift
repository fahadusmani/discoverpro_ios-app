//
//  GridCollectionViewCell.swift
//  AroundMeApplication
//
//  Created by Admin on 6/19/17.
//  Copyright © 2017 Mujadidia. All rights reserved.
//

import UIKit

class GridCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var gridImageView: UIImageView!
    
    
    @IBOutlet weak var gridLabel: UILabel!
    
    
}
