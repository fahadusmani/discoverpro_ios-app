//
//  GridCellModel.swift
//  AroundMeApplication
//
//  Created by Admin on 6/22/17.
//  Copyright © 2017 Mujadidia. All rights reserved.
//

import Foundation
import UIKit

class GridCellController
{
    var colorDataObject = ImageColor()
    var menuDataObject = Menu_Data()
    
    func applyCellAttributes(cell: GridCollectionViewCell, index: Int, isSearching : Bool, text : String) -> UICollectionViewCell
    {
        cell.layer.borderColor = colorSelect.color?.cgColor as! CGColor
        cell.layer.borderWidth = 0.4
        
        if(isSearching)
        {
            let img = UIImage(named: menuDataObject.menuIconDictionary[text]!)
            
            let coloredImg = colorDataObject.maskWithColor(img: img!, color: colorSelect.color!)
            
            cell.gridImageView.image = coloredImg
            cell.gridLabel.text = text
            cell.gridLabel.font = UIFont(name: "Noto Sans", size: 14.0)
            
            return cell
        }
            
        else
        {
            let img = UIImage(named: menuDataObject.menuIconDictionary[menuDataObject.menuName[index]]!)
            
            let coloredImg = colorDataObject.maskWithColor(img: img!, color: colorSelect.color!)
            
            cell.gridImageView.image = coloredImg
            cell.gridLabel.text = menuDataObject.menuName[index]
            cell.gridLabel.font = UIFont(name: "Noto Sans", size: 14.0)
            
            return cell
        }
    }
    
    
    func setDataToSend(isSearching : Bool, text : String, index: Int) -> [menuData]
    {
        var dataa = menuData()
        if(isSearching)
        {
            dataa.namee = text
            dataa.selectedKey = menuDataObject.menuKeyDictionary[text]
            dataa.icon = menuDataObject.menuIconDictionary[text]
            return [dataa]
        }
        else
        {
            dataa.namee = menuDataObject.menuName[index]
            
            dataa.selectedKey = menuDataObject.menuKeyDictionary[menuDataObject.menuName[index]]
            
            dataa.icon = menuDataObject.menuIconDictionary[menuDataObject.menuName[index]]
            return [dataa]
        }
    }
    
}
