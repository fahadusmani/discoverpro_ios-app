//
//  CustomCollectionCell.swift
//  AroundMeApplication
//
//  Created by Admin on 5/5/17.
//  Copyright © 2017 Mujadidia. All rights reserved.
//

import Foundation
import UIKit



class CustomCollectionCell: UICollectionViewCell  {
    
    @IBOutlet var collectionImageView: UIImageView!
    
    @IBOutlet var collectionImageTitleLbl: UILabel!
    
    
}
