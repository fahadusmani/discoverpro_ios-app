//
//  ListVc.swift
//  AroundMeApplication
//
//  Created by Admin on 4/18/17.
//  Copyright © 2017 Mujadidia. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import GooglePlacePicker
import NVActivityIndicatorView

class ListVc: UIViewController, UITableViewDelegate, UITableViewDataSource, CLLocationManagerDelegate, JsonRequestDelegate, ButtonCellDelegate,ListCellDelegate {
    
    var cellController = ListVcCellConroller()
     
    //class variables
    var alertObj = AlertViewws()
    var objColor = colorSelect()
    var loaderObject = Loader()
    var getDataArray = [menuData]()
    //var activityIndicatorView = UIActivityIndicatorView()
    var JsonRequestObj = JsonRequest()
    var receivedJsonData = [Info]()
    static var distanceUpdate = 0
    var activityIndicatorView1 : NVActivityIndicatorView!
    var isFirstPage = true
    var _receivedPlacesData : [ListDataArray]?
    static var distanceUnitKm = true
    var dataForDetailVc = [DetailDataArray]()
    
    var _getDataArray : [menuData]?  //get segue data from previous viewController
    {
        didSet{
            getDataArray = _getDataArray!
        }
    }
    
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var listTable: UITableView!
    
    static var lastPage = 0
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return (receivedJsonData.count)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return CGFloat(70)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cellIdentifier = "cell"
        var cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ListTableCell
        
        cell.listDelegate = self
        cell = cellController.applyCellAttributes(cell: cell, index: indexPath.row, data: getDataArray, jsonData: receivedJsonData)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        JsonRequestObj.updateShedule(pid: receivedJsonData[indexPath.row].id!)
        dataForDetailVc = cellController.setDataToSend(row: indexPath.row, array: receivedJsonData)
        performSegue(withIdentifier: "toDetailVc", sender: self)
    }
    
    //load more data if available when tableView scrolls down
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if indexPath.row == receivedJsonData.count-1
        {
            if(JsonData.nextPageToken != "")
            {
                isFirstPage = false
                activityIndicatorView1.startAnimating()
                self.JsonRequestObj.delegate = self
                self.JsonRequestObj.update(_key: (self.getDataArray[0].selectedKey)!)
            }
        }
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        if(HomeVc.isLocationEnabled == true)
        {
            objColor.setNavigationTitleImage(item: self.navigationItem)
            
            activityIndicatorView1 = loaderObject.showActivityIndicatory1(uiView: self.view)
            activityIndicatorView1.startAnimating()
            
            listTable.isHidden = true
            view.backgroundColor = colorSelect.settingsTitleColor
            
            receivedJsonData.removeAll()
            JsonRequestObj.delegate = self
            JsonRequestObj.update(_key: (getDataArray[0].selectedKey)!)
            
            listTable.tableFooterView = UIView(frame: .zero)
        }
            
        else
        {
            let alertController = alertObj.setLocationAlert()
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    
    func sendData(data: [Info]) //getting places data by delegate
    {
        DispatchQueue.main.async
            {
                self.receivedJsonData = data
                self.activityIndicatorView1.stopAnimating()
                
                if(self.receivedJsonData.isEmpty)
                {
                    let alert = self.alertObj.setPlacesAlert(key: self.getDataArray[0].selectedKey!)
                    self.present(alert, animated: true, completion: nil)
                }
                else
                {
                    self.listTable.isHidden = false
                    self.view.backgroundColor = UIColor.white
                    if(self.isFirstPage)
                    {
                        UIView.transition(with: self.listTable, duration: 0.7, options: .transitionCrossDissolve , animations: {self.listTable.reloadData()}, completion: nil)
                    }
                    else{
                        self.listTable.reloadData()
                        let indexPath =
                            NSIndexPath(row: self.receivedJsonData.count-19, section: 0)
                        self.listTable.scrollToRow(at: indexPath as IndexPath, at: .bottom, animated: true)
                    }
                }
        }
    }
    
    func sendData1(data: [ListDataArray])
    {}
    
    override func viewDidAppear(_ animated: Bool)
    {
        isFirstPage = true
        self.tabBarController?.tabBar.isHidden = false
        objColor.fetchColor()
        objColor.applyColor(nav: self.navigationController!, bar: self.tabBarController!)
        
        if(ListVc.distanceUpdate == 1 && ListVc.lastPage != 0)
        {
            receivedJsonData.removeAll()
            activityIndicatorView1 = loaderObject.showActivityIndicatory1(uiView: self.view)
            activityIndicatorView1.startAnimating()
            
            listTable.isHidden = true
            view.backgroundColor = colorSelect.settingsTitleColor
            JsonRequestObj.delegate = self
            JsonRequestObj.update(_key: (getDataArray[0].selectedKey)!)
        }
        ListVc.distanceUpdate = 0
        listTable.reloadData()
        ListVc.lastPage = 1
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if (segue.identifier == "toDetailVc")
        {
            var count = 4
            let dest = segue.destination as! DetailVc
            dest._getDataArray = dataForDetailVc
        }
    }
    
    func favTapped(cell: ButtonCell) {
        
    }
    func sharedTapped(cell: ButtonCell) {
        
    }
    func currenTTapped(cell: ButtonCell) {
        
    }
}
