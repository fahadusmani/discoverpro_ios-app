//
//  SegmentedCell.swift
//  AroundMeApplication
//
//  Created by Admin on 5/15/17.
//  Copyright © 2017 Mujadidia. All rights reserved.
//

import UIKit

protocol SegmentCellDelegate {
    func button(cell: SegmentedCell)
}


class SegmentedCell: UISegmentedControl {
    
    var segmentedDelegate : SegmentCellDelegate?
    
    
    
    @IBAction func segmentedIndex(_ sender: Any) {
        if let delegate = segmentedDelegate {
            delegate.button(cell: self)
        }
    }

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
