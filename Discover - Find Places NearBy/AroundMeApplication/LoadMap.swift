//
//  LoadMap.swift
//  AroundMeApplication
//
//  Created by Admin on 4/18/17.
//  Copyright © 2017 Mujadidia. All rights reserved.
//
import Foundation
import GoogleMaps
import GooglePlaces
import GooglePlacePicker

protocol LoadMapDelegate: class
{
    func sendPhotosData(data: [UIImage])
}


public class LoadMap :UIViewController,  CLLocationManagerDelegate
{
    var mapDelegate : LoadMapDelegate?
    static var currentLocation : CLLocation?
    var placesDetails = [placeData]()
    var googleMapView: GMSMapView!
    var locationManager: CLLocationManager!
    var placePicker: GMSPlacePicker!
    static var latitude : Double?
    static var longitude : Double?
    var selectedPlace: GMSPlace?
    var placesClient : GMSPlacesClient?
    var details = [placeData]()
    
    static var jsonData = [Info]()
    var logoImages = [UIImage]()
    
    func loadGMS()  //loading current location
    {
        self.locationManager = CLLocationManager()
        self.locationManager.delegate = self
        self.locationManager.requestAlwaysAuthorization()
        self.locationManager.startUpdatingLocation()
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        locationManager.distanceFilter = 50
    }
    
    
    func calculateDistance(lat : Double, long : Double) -> Double
    {       // Calculating Distance
        let newLoc = CLLocation(latitude: lat, longitude: long)
        var distance : Double?
        if(ListVc.distanceUnitKm == true)
        {
            distance = (LoadMap.currentLocation?.distance(from: newLoc))! / 1000
        }
        else
        {
            distance = (LoadMap.currentLocation?.distance(from: newLoc))! / 1609
        }
        distance = distance?.roundTo(places: 2)
        return distance!
    }
    
    public func locationManager(_ manager: CLLocationManager,
                                didUpdateLocations locations: [CLLocation])
    {
        let location:CLLocation = locations.last!
        LoadMap.latitude = location.coordinate.latitude
        LoadMap.longitude = location.coordinate.longitude
        LoadMap.currentLocation = location
    }
    
    func locationManager(_ manager: CLLocationManager,
                         didFailWithError error: NSError)
    {
        print("An error occurred while tracking location changes : \(error.description)")
        HomeVc.isLocationEnabled = false
    }
    
    func loadPhotos(pid : String)
    {
        let placeID = pid
        GMSPlacesClient.shared().lookUpPhotos(forPlaceID: placeID) { (photos, error) -> Void in
            
            DetailVc.logoImages.removeAll()
            self.logoImages.removeAll()
            if let error = error
            {
                print("Error: \(error.localizedDescription)")
            }
            else
            {
                
                if let firstPhoto = photos?.results
                {
                    self.loadImageForMetadata(photoMetadata: firstPhoto)
                }
            }
        }
    }
    
    func loadImageForMetadata(photoMetadata: [GMSPlacePhotoMetadata])
    {
        if(photoMetadata.count > 0)
        {
            for i in 0...(photoMetadata.count)-1
            {
                GMSPlacesClient.shared().loadPlacePhoto(photoMetadata[i], callback: {
                    (photo, error) -> Void in
                    if let error = error
                    {
                        print("Error: \(error.localizedDescription)")
                    }
                    else
                    {
                        DispatchQueue.main.async
                            {
                                DetailVc.img = photo!
                                DetailVc.logoImages.append(photo!)
                                self.logoImages.append(photo!)
                                self.mapDelegate?.sendPhotosData(data: self.logoImages)
                        } // dispatch
                    }
                }) //callback
            }//for
            
        }//if
        else
        {
            DetailVc.logoImages = []
            logoImages = []
            self.mapDelegate?.sendPhotosData(data: self.logoImages)
        }
    }
}

extension Double
{
    /// Rounds the double to decimal places value
    func roundTo(places:Int) -> Double
    {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
}

