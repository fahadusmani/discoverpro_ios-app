//
//  ButtonsTableViewCell.swift
//  AroundMeApplication
//
//  Created by Admin on 4/25/17.
//  Copyright © 2017 Mujadidia. All rights reserved.
//

import Foundation
import UIKit

protocol ButtonsDelegate{
    func closeFriendsTapped(at index:IndexPath)
}


class ButtonsTableViewCell: UITableViewCell
{
    var delegate: ButtonsDelegate!
    
    @IBOutlet weak var closeFriendsBtn: UIButton!
    
    var indexPath:IndexPath!
    
    @IBAction func closeFriendsAction(_ sender: UIButton)
    {
        self.delegate?.closeFriendsTapped(at: indexPath)
    }
    
}
