//
//  CoreDataModel.swift
//  AroundMeApplication
//
//  Created by Admin on 4/25/17.
//  Copyright © 2017 Mujadidia. All rights reserved.
//

import Foundation
import UIKit
import CoreData


class CoreDataModel
{
    static var people : [NSManagedObject] = []
    
    func fetchData() -> [DetailDataArray]
    {
        var favData = [DetailDataArray]()
        let AppDel:AppDelegate = (UIApplication.shared.delegate as! AppDelegate)
        
        let context =
            AppDel.persistentContainer.viewContext
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Favorite")
        
        request.returnsObjectsAsFaults = false
        
        let results: NSArray = try! context.fetch(request) as NSArray
        
        if(results.count > 0)
        {
            var data = DetailDataArray()
            
            let x = (results.count) - 1
            
            for i in 0...x
            {
                let res = results[i] as! NSManagedObject
                let nm = res.value(forKey: "name") as! String
                let locationName : String?
                
                if(res.value(forKey: "location") != nil)
                {
                    locationName = res.value(forKey: "location") as! String
                }
                else
                {
                    locationName = ""
                }
                
                let lat = res.value(forKey: "latitude") as! Double
                let long = res.value(forKey: "longitude") as! Double
                let placeID = res.value(forKey: "placeID") as! String
                let distance = res.value(forKey: "distance") as! Double
                let timings : [String]?
                if res.value(forKey: "timings") != nil
                {
                    timings = res.value(forKey: "timings") as! [String]
                }
                else {timings = []}
                
                let phonee : String?
                let web : String?
                
                if res.value(forKey: "website") != nil
                {
                    web = res.value(forKey: "website") as! String
                }
                else { web = nil }
                
                if res.value(forKey: "phone") != nil
                {
                    phonee = res.value(forKey: "phone") as! String
                }
                else { phonee = nil }
                
                let open : Bool?
                if res.value(forKey: "openNow") != nil
                {
                    open = res.value(forKey: "openNow") as! Bool
                }
                else { open = nil }
                
                data.name = nm
                data.latitude = lat
                data.longitude = long
                data.location = locationName
                data.openNow = open
                data.phone = phonee
                data.website = web
                data.placeId = placeID
                data.distance = distance
                data.timings = timings
                favData.append(data)
            }
        }
        return favData
    }
    
    
    func deleteData(name: String)
    {
        let AppDel:AppDelegate = (UIApplication.shared.delegate as! AppDelegate)
        
        let context =
            AppDel.persistentContainer.viewContext
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Favorite")
        
        request.returnsObjectsAsFaults = false
        
        let results: NSArray = try! context.fetch(request) as NSArray
        print(results)
        
        let resultData = results as! [Favorite]
        
        for object in resultData
        {
            if(object.name == name)
            {
                context.delete(object)
            }
        }
        do
        {
            try context.save()
        }
        catch let error as NSError
        {
            print("Could not save \(error), \(error.userInfo)")
        }
        catch
        {
        }
    }
}
